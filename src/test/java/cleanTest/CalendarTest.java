package cleanTest;

import activities.calendar.AlarmaScreen;
import activities.calendar.DateScreen;
import activities.calendar.MainScreen;
import controlAppium.Button;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;


import java.net.MalformedURLException;

public class CalendarTest {

    private MainScreen mainScreen = new MainScreen();
    private AlarmaScreen alarmaScreen = new AlarmaScreen();


    @Test
    public void verifyConfigAlarm() throws MalformedURLException {
        String hour = "07:30";

        mainScreen.addAlarmButton.click();

        // elegir cambiar hora

        mainScreen.addHour.click();
        mainScreen.lefthour.click();
        mainScreen.AcceptButton.click();
        mainScreen.AcceptAlarmButton.click();
        alarmaScreen.agendaButton.click();

        Assert.assertEquals("ERROR! la alarma no fue creada a las " + hour + "!",hour,alarmaScreen.nameHourLabel.getText());



     //   Assert.assertEquals("ERROR! task was not created",title,mainScreen.nameTaskLabel.getText());
    }
   @After
    public void verifyeditalarm() throws MalformedURLException{
        // visualizar alarmas creadas

        String hour = "10:30";

        alarmaScreen.hourButton.click();
        mainScreen.addHour.click();
        mainScreen.lefthour.click();
        mainScreen.lefthour.click();
        mainScreen.lefthour.click();
        mainScreen.AcceptButton.click();
        mainScreen.AcceptAlarmButton.click();

       Assert.assertEquals("ERROR! la alarma no fue actualizada a las " + hour + "!",hour,alarmaScreen.nameHourLabel.getText());

   }

}
