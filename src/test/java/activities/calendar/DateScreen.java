package activities.calendar;

import controlAppium.Button;
import org.openqa.selenium.By;

public class DateScreen {
    public Button dateButton = new Button(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.TextView[2]"));
    public Button pickerButton = new Button(By.xpath("android:id/day_picker_selector_layout"));

    public DateScreen(){}
}
